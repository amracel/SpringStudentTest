package gov.uscourts.vaeb.springtest.dao;

import gov.uscourts.vaeb.springtest.pojo.Student;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class StudentJDBCTemplate implements StudentDAO {
	
	@Autowired
	private DataSource dataSource;

	public void create(Student student) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String SQL = "INSERT INTO Student (name, age) values(?,?)";
		
		if(student.getName() != null) {
			jdbcTemplate.update(SQL, student.getName(),student.getAge());
		}
		
	}

	public Student getStudent(Integer id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * from Student where id = " + id;
		List<Student> studentList = new ArrayList<Student>();
		studentList = jdbcTemplate.query(sql, new StudentRowMapper());
		return studentList.get(0);
	}

	public List<Student> listStudents() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * from Student";
		List<Student> students = (List<Student>) jdbcTemplate.query(sql, new StudentRowMapper());
		return students;
	}

	public void delete(Integer id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "DELETE FROM Student WHERE id = ?";
		jdbcTemplate.update(sql, id);
		System.out.println("Deleted record with ID " + id);
		
	}

	public void update(Integer id, Integer age) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "UPDATE Student SET age = ? where id = ?";
		jdbcTemplate.update(sql, age, id);
		System.out.println("Updated record with id = " + id);
		
	}

	public void create(String name, Integer age) {
		// TODO Auto-generated method stub
		
	}

}

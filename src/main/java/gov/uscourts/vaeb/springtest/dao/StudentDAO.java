package gov.uscourts.vaeb.springtest.dao;

import gov.uscourts.vaeb.springtest.pojo.Student;

import java.util.List;

import javax.sql.DataSource;

public interface StudentDAO {
	
	
	/**
	 * Create a record in the Student table.
	 * @param name
	 * @param age
	 */
	public void create(Student student);

	/**
	 * Create a record in the Student table.
	 * @param name
	 * @param age
	 */
	public void create(String name, Integer age);
	
	/**
	 * Get a student, based on ID
	 * @param id
	 * @return
	 */
	public Student getStudent(Integer id);
	
	/**
	 * Get a list of all students in the database.
	 * @return
	 */
	public List<Student> listStudents();
	
	/**
	 * Remove the student with the given ID
	 * @param id
	 */
	public void delete(Integer id);
	
	/**
	 * Update a record in the student table.
	 * @param id
	 * @param age
	 */
	public void update(Integer id, Integer age);
}

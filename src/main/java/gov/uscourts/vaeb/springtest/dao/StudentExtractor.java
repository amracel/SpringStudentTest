package gov.uscourts.vaeb.springtest.dao;

import gov.uscourts.vaeb.springtest.pojo.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class StudentExtractor implements ResultSetExtractor<Student> {

	public Student extractData(ResultSet rs) throws SQLException,
			DataAccessException {
		Student student = new Student();
		student.setAge(rs.getInt("age"));
		student.setId(rs.getInt("id"));
		student.setName(rs.getString("name"));
		
		return student;
	}
	
	

}

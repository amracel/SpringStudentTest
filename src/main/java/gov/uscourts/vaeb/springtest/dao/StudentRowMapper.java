package gov.uscourts.vaeb.springtest.dao;

import org.springframework.jdbc.core.RowMapper;

import gov.uscourts.vaeb.springtest.pojo.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentRowMapper implements RowMapper<Student> {

	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		StudentExtractor extractor = new StudentExtractor();
		return extractor.extractData(rs);
	}
}

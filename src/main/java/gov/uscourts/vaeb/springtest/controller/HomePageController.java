package gov.uscourts.vaeb.springtest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.uscourts.vaeb.springtest.dao.StudentDAO;
import gov.uscourts.vaeb.springtest.pojo.Student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomePageController {
	
	@Autowired
	StudentDAO studentService;
	
	@RequestMapping("/newStudent")
	public ModelAndView addStudent(@ModelAttribute Student student) {
		return new ModelAndView("addStudent");
	}
	
	@RequestMapping("/insert") 
	public String insertData(@ModelAttribute Student student) {
		if((student != null) ) {
			studentService.create(student);
		}
		return "redirect:/getList";
	}
	
	@RequestMapping("/getList")
	public ModelAndView getStudentList() {
		List<Student> studentList = studentService.listStudents();
		return new ModelAndView("studentList","studentList", studentList);
	}
	
	@RequestMapping("/edit")
	public ModelAndView editStudent(@RequestParam String id, @ModelAttribute Student student) {
		student = studentService.getStudent(new Integer(id));
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("student", student);
		return new ModelAndView("edit","map",map);
		
	}
	
	@RequestMapping("/update")
	public String updateStudent(@ModelAttribute Student student) {
		studentService.update(student.getId(), student.getAge());
		return "redirect:/getList";
	}
	
	@RequestMapping("/delete")
	public String deleteUser(@RequestParam String id) {
		System.out.println("id = " + id);
		studentService.delete(new Integer(id));
		return "redirect:/getList";
	}

}

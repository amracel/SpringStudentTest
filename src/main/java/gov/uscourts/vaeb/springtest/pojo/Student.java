package gov.uscourts.vaeb.springtest.pojo;

public class Student {

	private Integer age;
	private String name;
	private Integer id;
	
	public void setAge(Integer arg) {
		age = arg;
	}
	public Integer getAge() {
		return age;
	}
	public void setName(String arg) {
		name = arg;
	}
	public String getName() {
		return name;
	}
	public void setId(Integer arg) {
		id = arg;
	}
	public Integer getId() {
		return id;
	}
}

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Spring JDBC Sample</title>
<style>
body {
	font-size: 20px;
	font-family: Calibri;
}

td {
	font-size: 15px;
	color: black;
	width: 100px;
	height: 22px;
	text-aligh: left;
}

.heading {
	font-size: 18px;
	font: bold;
	border: thick;
}
</style>
</head>
<body>
	<h3>Spring JDBC Sample | Student Registration</h3>

	<div>
		<form:form method="post" action="insert" modelAttribute="student">
			<table>
				<tr>
					<td>Name:</td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td>Age:</td>
					<td><form:input path="age" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Save" /></td>
				</tr>
				<tr>
					<td colspan='2'><a href="getList">Click Here to See List
							of Students</a></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>